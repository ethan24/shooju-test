__author__ = 'ecowan'

import os
import ConfigParser


class CredentialsConfigParser(object):

    def __init__(self):
        self.config_parser = ConfigParser.ConfigParser()
        self.abs_file_path = os.path.join(os.path.dirname(__file__), 'credentials.cfg')
        self._credentials = None

    @property
    def credentials(self):
        if self._credentials:
            return self._credentials
        self.config_parser.read(self.abs_file_path)
        for section_name in self.config_parser.sections():
            if section_name == 'Credentials':
                self._credentials = dict(self.config_parser.items(section_name))
        return self._credentials

credentials = CredentialsConfigParser().credentials
