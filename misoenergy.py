__author__ = 'ecowan'

from datetime import datetime
from collections import OrderedDict

import requests
from shooju import Connection, sid, Point

from credentials_config import credentials


VERBOSE = True


class MisoEnergyImporter(object):

    def __init__(self, server='test1', user_credentials=None):
        if not user_credentials:
            self.credentials = credentials
        else:
            self.credentials = user_credentials
        self.data = None
        self.server = server
        self.connection = Connection(self.server, **self.credentials)
        self.line_delimiter = '\n'
        self.data_delimiter = ','
        self.structure = OrderedDict({
            'INTERVAL': self.generate_datetime,
            'CPNODE': str,
            'LMP': float,
            'MLC': float,
            'MCC': float
        })

    def format_line(self, line_dict):
        return OrderedDict((k, self.structure[k](v)) for k, v in line_dict.items())

    @staticmethod
    def generate_datetime(time_string):
        return datetime.strptime(time_string, '%Y-%m-%dT%H:%M:%S')

    def generate_series_id(self, *args):
        return sid("users", self.credentials['user'], 'miso', *args)

    def get_data(self):
        try:
            csv_file = open('data/RealTimeFiveMinuteExPostIntervalsDataRollingMarketDay.csv', 'r')
            self.data = csv_file.read()
        except IOError:
            data = requests.get(
                'https://www.misoenergy.org/ria/ELMPRealTimeFiveMinuteExPostIntervals.aspx?period=rollingmarketday'
            )
            self.data = data.content

    def parse_data(self, batch_size=200000):
        batch_job = self.connection.register_job('test', batch_size=batch_size)
        if not self.data:
            self.get_data()
        lines = self.data.split(self.line_delimiter)
        labels = lines[0].strip().split(self.data_delimiter)
        lines = [x for x in lines[1:] if len(x) > 0]
        for i, line in enumerate(lines):
            line = line.strip().split(self.data_delimiter)
            line_dict = OrderedDict((k, v) for (k, v) in zip(labels, line))
            formatted_line = self.format_line(line_dict)
            cp_node = formatted_line['CPNODE']
            date = formatted_line['INTERVAL']
            for label, data_point in formatted_line.items():
                series_id = self.generate_series_id(cp_node, label)
                if type(data_point) == float:
                    batch_job.put_point(series_id, Point(date, data_point))
            if VERBOSE:
                print i, cp_node
        batch_job.submit()
        if VERBOSE:
            print 'Series Points: %s' % series_id
            print self.connection.pd.get_points(series_id)





