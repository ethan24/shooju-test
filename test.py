__author__ = 'ecowan'

import unittest
from collections import OrderedDict
import datetime

from misoenergy import MisoEnergyImporter


class TestMisoImporter(unittest.TestCase):

    def setUp(self):
        self.misoenergy_importer = MisoEnergyImporter()

    def test_format_line(self):
        labels = "INTERVAL,CPNODE,LMP,MLC,MCC".split(',')
        line = "2016-01-08T00:00:00,AMMO.OSAGE8,17.97,-1.71,0.00".split(',')
        line_dict = dict(zip(labels, line))
        formatted_line = self.misoenergy_importer.format_line(line_dict)
        self.assertEqual(formatted_line, OrderedDict([('LMP', 17.97), ('MLC', -1.71),
                                                      ('INTERVAL', datetime.datetime(2016, 1, 8, 0, 0)),
                                                      ('MCC', 0.0), ('CPNODE', 'AMMO.OSAGE8')]))

    def test_datetime(self):
        date_string = "2016-01-08T00:00:00"
        datetime_object = self.misoenergy_importer.generate_datetime(date_string)
        self.assertEqual(datetime_object, datetime.datetime(2016, 1, 8, 0, 0))

    def test_series_id(self):
        series_id = self.misoenergy_importer.generate_series_id("test_series")
        self.assertEqual(series_id, 'users\\ethan\\miso\\test_series')

